import os
import asyncio
import argparse
import collections
from typing import List

import yaml
from aiohttp import web, ClientSession, client_exceptions

Neighbour = collections.namedtuple('Neighbour', 'name, host, port, url')


class ServerConfig:
    def __init__(self,
                 host: str, port: int,
                 directory: str, save_found: str,
                 neighbours: List[Neighbour]):
        self.host = host
        self.port = port
        self.directory = directory
        self.save_found = save_found
        self.neighbours = neighbours

    @classmethod
    def load_from_yaml(cls, config_path):
        with open(config_path, 'rb') as config_file:
            config = yaml.load(config_file)

        def parse_neighbours():
            n_list = []
            for n_name, n_conf in config['neighbours'].items():
                protocol = 'http://'
                host, port = n_conf['host'], int(n_conf.get('port'))
                url = protocol + f'{host}:{port}' if port else host
                n_list.append(Neighbour(n_name, host, port, url))
            return n_list

        return cls(config['host'], config['port'],
                   config['directory'], config['save_found'],
                   parse_neighbours())

    @classmethod
    def load_from_file(cls, config_path):
        _config_loaders = {
            '.yml': cls.load_from_yaml,
            '.yaml': cls.load_from_yaml
        }
        _, ext = os.path.splitext(config_path)
        try:
            return _config_loaders[ext](config_path)
        except KeyError:
            raise TypeError(f'{ext} config type in unsupported') from None


class StaticAsyncServer:
    """
    Static server in async way
    """
    n_check_endpoint = 'neighbour_check'
    n_download_endpoint = 'neighbour_download'

    def __init__(self, config: ServerConfig, loop=None):
        if loop is None:
            loop = asyncio.get_event_loop()
        self.loop = loop

        self.config = config
        self.app = web.Application()
        self.setup_routes()
        self.runner = None
        self.site = None

    def run(self):
        web.run_app(self.app, host=self.config.host, port=self.config.port)

    def setup_routes(self):
        self.app.add_routes([
            web.get('/{file_name}', self.main_download),
            web.get(f'/{self.n_download_endpoint}/{{file_name}}', self.neighbour_download),
            web.get(f'/{self.n_check_endpoint}/{{file_name}}', self.neighbour_check)
        ])

    async def prepare_file_response(self, file_bytes: bytes):
        """
        Feature for text files

        Text files will be sent as the text, other - as binaries for downloading
        """
        # think about checking file ext
        try:
            file_text = await self.loop.run_in_executor(None, file_bytes.decode, 'utf8')
            return web.Response(text=file_text)
        except UnicodeDecodeError:
            return web.Response(body=file_bytes)

    async def main_download(self, request):
        """ File downloading handler """
        file_name = request.match_info.get('file_name')

        search_in_neighbour = False

        file_bytes = await self.get_file_from_storage(file_name)
        if not file_bytes:
            file_bytes = await self.ask_neighbours(file_name)
            search_in_neighbour = True

        if not file_bytes:
            logging.debug('No such file anywhere')
            raise web.HTTPNotFound(text=f'There is no file with name {file_name!r}')

        if search_in_neighbour and self.config.save_found:
            await self.loop.run_in_executor(None,
                                            self.write_file_to_storage,
                                            file_name, file_bytes)

        return await self.prepare_file_response(file_bytes)

    async def neighbour_download(self, request):
        """ Handler for download requests from neighbours """
        file_name = request.match_info.get('file_name')

        logging.debug(f'Got file download request from neighbour')
        file_bytes = await self.get_file_from_storage(file_name)
        if not file_bytes:
            raise web.HTTPNotFound(text=f'Sry, neighbour, there is no file with name {file_name!r}')

        return await self.prepare_file_response(file_bytes)

    async def neighbour_check(self, request):
        """ Handler for check requests from neighbours """
        file_name = request.match_info.get('file_name')
        file_path = self.check_file_in_storage(file_name)
        if not file_path:
            raise web.HTTPNotFound(text=f'Sry, neighbour, there is no file with name {file_name!r}')
        return web.Response(text='File found')

    def check_file_in_storage(self, file_name):
        file_path = os.path.join(self.config.directory, file_name)
        if not os.path.exists(file_path):
            return None
        return file_path

    @staticmethod
    def read_file_from_storage(path):
        with open(path, 'rb') as file:
            return file.read()

    def write_file_to_storage(self, file_name, file_bytes):
        file_path = os.path.join(self.config.directory, file_name)
        with open(file_path, 'wb') as file:
            return file.write(file_bytes)

    async def get_file_from_storage(self, file_name):
        logging.debug(f'Checking storage for {file_name}')
        file_path = self.check_file_in_storage(file_name)
        if not file_path:
            logging.debug('No such file in storage')
            return None
        logging.debug('File found')
        file_bytes = await self.loop.run_in_executor(None,
                                                     self.read_file_from_storage,
                                                     file_path)
        return file_bytes

    async def ask_neighbours(self, file_name):
        logging.debug(f'Checking neighbours for file {file_name}')
        tasks = []
        async with ClientSession() as session:
            for n in self.config.neighbours:
                task = asyncio.ensure_future(self.ask_neighbour(n, file_name, session))
                tasks.append(task)

            responses = await asyncio.gather(*tasks)

            # finding not None response
            for neigbour, result in filter(lambda n_and_res: bool(n_and_res[1]), responses):
                return await self.download_from_neighbour(neigbour, file_name, session)
            else:
                return None

    async def download_from_neighbour(self, neighbour: Neighbour, file_name, session: ClientSession):
        download_url = f'{neighbour.url}/{self.n_download_endpoint}/{file_name}'
        logging.debug(f'Downloading neighbour {file_name!r} from {neighbour.name}')
        async with session.get(download_url) as response:
            if response.status == 200:
                return await response.read()
            return None

    async def ask_neighbour(self, neighbour: Neighbour, file_name, session: ClientSession):
        asking_url = f'{neighbour.url}/{self.n_check_endpoint}/{file_name}'
        logging.debug(f'Asking neighbour {neighbour.name} for {file_name!r}')
        try:
            async with session.get(asking_url) as response:
                if response.status == 200:
                    logging.debug(f'File {file_name!r} is FOUND at neighbour {neighbour.name}')
                    return neighbour, await response.read()
                logging.debug(f'File {file_name!r} not found at neighbour {neighbour.name}')
                return neighbour, None
        except client_exceptions.ClientConnectionError:
            logging.debug(f'Neighbour {neighbour.name} is down')
            return neighbour, None


def create_args_parser():
    p = argparse.ArgumentParser(description='Serves files in async way')
    p.add_argument('config_path', help='path to config for server daemon')
    return p


if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG)

    parser = create_args_parser()
    args = parser.parse_args()
    conf = ServerConfig.load_from_file(args.config_path)

    async_loop = asyncio.get_event_loop()
    server = StaticAsyncServer(conf, async_loop)

    server.run()
